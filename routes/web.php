<?php

use App\Http\Controllers\DataSetController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KoperasiController;
use App\Http\Controllers\LuasPerkebunanController;
use App\Http\Controllers\ResourceFileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::resource('luas_perkebunan',LuasPerkebunanController::class);
Route::post('luas_perkebunan_satudata',[LuasPerkebunanController::class,'sendToSatuData'])->name('luas_perkebunan.satudata');
Route::resource('datasets',DataSetController::class);
Route::resource('resource_files',ResourceFileController::class);
Route::get('data_koperasi',[KoperasiController::class,'index'])->name('koperasi.index');
Route::post('koperasi_satudata',[KoperasiController::class,'sendToSatuData'])->name('koperasi.satudata');

