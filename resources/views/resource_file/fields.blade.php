<div class="form-group">
    <label>Title:</label>
    <div class="position-relative">
        <input class="form-control" type="text" name="title" value="{{$resourceFile->title}}">
    </div>
</div>
<div class="form-group">
    <label>Pilih Dataset:</label>
    <div class="position-relative">
        <select name="dataset_id" class="form-control">
            @foreach($dataset as $item)
                <option value="{{$item->id}}">{{$item->title}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label>File (XLS/CSV):</label>
    <div class="position-relative">
        <input class="form-control" type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, .csv, application/vnd.ms-excel" name="file"><span>{{$resourceFile->file}}</span>
    </div>
</div>
<div class="form-group">
    <label>Description:</label>
    <div class="position-relative">
        <textarea class="form-control" name="description" rows="8">{{$resourceFile->description}}</textarea>
    </div>
</div>
<div class="form-actions center">
    <a href="{!! route('resource_files.index') !!}" class="btn btn-danger">Batal</a>
    <button type="submit" class="btn btn-success mr-1">Simpan</button>
</div>

