<table class="table table-hover table-bordered table-striped default">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Resource id</th>
        <th>Title</th>
        <th>File</th>
        <th>Description</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($resource_files as $item)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $item->resource_id !!}</td>
            <td>{!! $item->title !!}</td>
            <td><a href="{!! $item->file !!}">{!! $item->file !!}</a></td>
            <td>{!! $item->description !!}</td>
            <td class="text-center">
                <form action="{{route('resource_files.destroy',$item->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{!! route('resource_files.edit', [$item->id]) !!}" class="btn btn-sm btn-outline-warning"><i class="fa fa-edit"></i></a>
                        <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                    </div>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
