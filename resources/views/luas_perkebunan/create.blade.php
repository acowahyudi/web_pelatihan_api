@extends('adminlte::page')
@section('content')
    <div class="content-body">
        <section id="horizontal-form-layouts">
            <div class="row">
                <div class="col-md-12">
                    <div class="card overflow-hidden">
                        <div class="card-content">
                            <div class="media align-items-stretch">
                                <div class="bg-green p-2 text-center">
                                    <i class="fa fa-edit fa-2x text-white text-center"></i>
                                </div>
                                <div class="media-body p-1">
                                    <span class="green h3">Input Data Luas Perkebunan</span><br>
                                    <span style="margin-top: -5px">Membuat Data Baru</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collpase show">
                            <div class="card-body">
                                <form class="form" action="{{route('luas_perkebunan.store')}}" method="POST">
                                    @csrf
                                    <div class="form-body">
                                        @include('fields')
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
