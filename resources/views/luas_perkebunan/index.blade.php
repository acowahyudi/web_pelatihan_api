@extends('adminlte::page')
@section('content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="border-left-green box-shadow-1 rounded">
                        <div class="card-content ">
                            <div class="card-body card-dashboard">
                                <div class="row mb-2">
                                    <div class="col-9 media-body mb-2">
                                        <div class="content-header-left breadcrumb-new">
                                            <span class="content-header-title mb-0 d-inline-block h3"><b>Data Luas Perkebunan</b></span>
                                        </div>
                                    </div>
                                    <div class="col-3 text-right">
                                        <a href="{!! route('luas_perkebunan.create') !!}" class="btn btn-sm btn-outline-success">Tambah Data</a>
                                    </div>
                                </div>
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-default-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif
                                @if ($message = Session::get('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endif

                                @include('table')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card p-3">
                    <span class="h4"><b>Kirim ke Satu Data</b></span>
                    <form action="{{route('luas_perkebunan.satudata')}}" method="post">
                        @csrf
                        <div class="row m-2">
                            <label>Pilih Dataset:</label>
                            <div class="col-9">
                                <select name="dataset_id" class="form-control">
                                    @foreach($dataset as $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <span>
                            <button type="submit" class="btn btn-sm btn-outline-primary" onclick="return confirm('Apakah anda yakin untuk mengirim ke satu data?')">Kirim ke Satu Data</button>
                        </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
