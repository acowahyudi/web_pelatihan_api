<!-- Nama Field -->
<div class="form-group">
    <label>Kebun:</label>
    <div class="position-relative">
        <input class="form-control" type="text" name="kebun" value="{{$luasPerkebunan->kebun}}">
    </div>
</div>


<!-- Pengertian Field -->
<div class="form-group">
    <label>Luas Lahan:</label>
    <div class="position-relative">
        <input class="form-control" type="text" name="luas_lahan" value="{{$luasPerkebunan->luas_lahan}}">
    </div>
</div>

<!-- Sudah di modifikasi -->
<!-- Submit Field -->
<div class="form-actions center">
    <a href="{!! route('luas_perkebunan.index') !!}" class="btn btn-danger">Batal</a>
    <button type="submit" class="btn btn-success mr-1">Simpan</button>
</div>

