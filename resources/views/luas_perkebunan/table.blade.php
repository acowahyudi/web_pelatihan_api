<!-- Sudah di modifikasi untuk Edit,Lihat,Hapus -->
<table class="table table-hover table-bordered table-striped default">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Kebun</th>
        <th>Luas Lahan</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($luas_perkebunan as $item)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $item->kebun !!}</td>
            <td>{!! $item->luas_lahan !!} Ha</td>
            <td class="text-center">
                <form action="{{route('luas_perkebunan.destroy',$item->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="btn-group" role="group" aria-label="Basic example">
                        {{--<a href="{!! route('luas_perkebunan.show', [$item->id]) !!}" class="btn btn-sm btn-outline-success"><i class="fa fa-eye"></i></a>--}}
                        <a href="{!! route('luas_perkebunan.edit', [$item->id]) !!}" class="btn btn-sm btn-outline-warning"><i class="fa fa-edit"></i></a>
                        <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                    </div>
                </form>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

