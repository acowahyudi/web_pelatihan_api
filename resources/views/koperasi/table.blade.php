<table class="table table-hover table-bordered table-striped table-responsive">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        @foreach($koperasi['fields'] as $field)
            <th>{{$field['id']}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($koperasi['records'] as $item)
        <tr>
            <td>{{$no++}}</td>
            <td>{{$item['nama_koperasi']}}</td>
            <td>{{$item['luas_areal']}}</td>
            <td>{{$item['luas_lahan_tanam_telah_produksi']}}</td>
            <td>{{$item['luas_lahan_tanam_belum_produksi']}}</td>
            <td>{{$item['total_produksi']}}</td>
            <td>{{$item['produktifitas_lahan']}}</td>
            <td>{{$item['jml_pekerja']}}</td>
            <td>{{$item['jml_pupuk']}}</td>
            <td>{{$item['jml_benih']}}</td>
            <td>{{$item['jml_persil']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

