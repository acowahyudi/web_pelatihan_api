<div class="form-group">
    <label>Title/Judul Dataset:</label>
    <div class="position-relative">
        <input class="form-control" type="text" name="title" value="{{$dataset->title}}">
    </div>
</div>
<div class="form-group">
    <label>Bahasa:</label>
    <div class="position-relative">
        <input class="form-control" type="text" name="language" value="{{$dataset->language}}">
    </div>
</div>
<div class="form-group">
    <label>Cakupan:</label>
    <div class="position-relative">
        <input class="form-control" type="text" name="cakupan" value="{{$dataset->cakupan}}">
    </div>
</div>
<div class="form-group">
    <label>Tahun:</label>
    <div class="position-relative">
        <input class="form-control" type="number" name="tahun" value="{{$dataset->tahun}}">
    </div>
</div>
<div class="form-group">
    <label>Note/Keterangan:</label>
    <div class="position-relative">
        <textarea class="form-control" name="notes" rows="8">{{$dataset->notes}}</textarea>
    </div>
</div>
<div class="form-actions center">
    <a href="{!! route('datasets.index') !!}" class="btn btn-danger">Batal</a>
    <button type="submit" class="btn btn-success mr-1">Simpan</button>
</div>

