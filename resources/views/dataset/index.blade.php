@extends('adminlte::page')
@section('content')
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="border-left-pink border-left-6 box-shadow-1 rounded">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="row mb-2">
                                    <div class="col-10 media-body mb-2">
                                        <div class="content-header-left breadcrumb-new">
                                            <span class="content-header-title mb-0 d-inline-block h3"><b>Dataset</b></span>
                                        </div>
                                    </div>
                                    <div class="col-2 text-right">
                                        <a href="{!! route('datasets.create') !!}" class="btn btn-sm btn-outline-success">Tambah Data</a>
                                    </div>
                                </div>
                                @include('dataset.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
