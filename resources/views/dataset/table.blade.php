<table class="table table-hover table-bordered table-striped default">
    <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
    </colgroup>
    <thead>
    <tr>
        <th><code>#</code></th>
        <th>Package id</th>
        <th>Title</th>
        <th>Organisasi id</th>
        <th>Bahasa</th>
        <th>Tahun</th>
        <th>Cakupan</th>
        <th>Keterangan</th>
        <th style="text-align: center">Action</th>
    </tr>
    </thead>
    <tbody>
    @php
        $no = 1;
    @endphp
    @foreach($dataset as $item)
        <tr>
            <td>{!! $no++ !!}</td>
            <td>{!! $item->package_id !!}</td>
            <td>{!! $item->title !!}</td>
            <td>{!! $item->owner_org !!}</td>
            <td>{!! $item->language !!}</td>
            <td>{!! $item->tahun !!}</td>
            <td>{!! $item->cakupan !!}</td>
            <td>{!! $item->notes !!}</td>
            <td class="text-center">
                <form action="{{route('datasets.destroy',$item->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{!! route('datasets.edit', [$item->id]) !!}" class="btn btn-sm btn-outline-warning"><i class="fa fa-edit"></i></a>
                        <button type="submit" class="btn btn-sm btn-outline-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                    </div>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
