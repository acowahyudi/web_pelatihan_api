<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResourceFile extends Model
{
    use HasFactory;
    protected $table = 'resource_file';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'file',
        'title',
        'description',
        'dataset_id',
        'resource_id',
    ];

    public function dataset()
    {
        return $this->belongsTo(\App\Models\Dataset::class, 'dataset_id');
    }
}
