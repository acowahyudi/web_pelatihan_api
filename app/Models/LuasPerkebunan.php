<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LuasPerkebunan extends Model
{
    use HasFactory;
    public $table = 'luas_perkebunan';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'kebun',
        'luas_lahan',
    ];
}
