<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dataset extends Model
{
    use HasFactory;
    protected $table = 'dataset';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'package_id',
        'title',
        'owner_org',
        'language',
        'notes',
        'tahun',
        'cakupan'
    ];

    public function resourceFile()
    {
        return $this->hasMany(\App\Models\ResourceFile::class, 'dataset_id');
    }
}
