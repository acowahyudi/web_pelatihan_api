<?php

namespace App\Http\Controllers;

use App\Models\Dataset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DataSetController extends Controller
{
    public function index()
    {
        $dataset = Dataset::all();
        return view('dataset.index',compact('dataset'));
    }

    public function create()
    {
        $dataset = new Dataset();
        return view('dataset.create',compact('dataset'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['name'] = str_replace(' ', '-', strtolower($input['title']));
        $input['metadata_language'] = $input['language'];
        $input['owner_org'] = config('app.owner_org');
        $response = Http::withHeaders([
            'Authorization' => config('app.api_key_satudata'),
        ])->post('https://data.kutaitimurkab.go.id/api/3/action/package_create', $input);
        $input['package_id'] = $response['result']['id'];

        Dataset::create($input);

        return redirect(route('datasets.index'));
    }

    public function edit(Dataset $dataset)
    {
        return view('dataset.edit',compact('dataset'));
    }

    public function update(Request $request, Dataset $dataset)
    {
        $input = $request->all();
        $dataset->update($input);

        $input['id'] = $dataset->package_id;
        $input['name'] = str_replace(' ', '-', strtolower($input['title']));
        $input['metadata_language'] = $input['language'];
        $response = Http::withHeaders([
            'Authorization' => config('app.api_key_satudata'),
        ])->post('https://data.kutaitimurkab.go.id/api/3/action/package_update', $input);

        return redirect(route('datasets.index'));
    }

    public function destroy(Dataset $dataset)
    {
        $dataset->delete();
        $input['id'] = $dataset->package_id;
        $response = Http::withHeaders([
            'Authorization' => config('app.api_key_satudata'),
        ])->post('https://data.kutaitimurkab.go.id/api/3/action/package_delete', $input);

        return redirect(route('datasets.index'));
    }
}
