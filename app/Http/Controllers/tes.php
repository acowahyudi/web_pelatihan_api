<?php

class ResourceFileAPIController extends Controller
{
    public function index()
    {
        $resource_file = ResourceFile::with('dataset')->get();
        return response()->json([
            'message' => 'Data berhasil mengambil data resource File',
            'status' => 'sukses',
            'data' => $resource_file], 200);
    }
    public function store(Request $request)
    {
        $input = $request->except('file');
        $dataset = Dataset::find($input['dataset_id']);
        $input['package_id'] = $dataset->package_id;
        $input['language'] = $dataset->language;
        $input['metadata_language'] = $dataset->language;
        $input['name'] = str_replace(' ', '-', strtolower($input['title']));
        $input['format'] = $request->file('file')->getClientOriginalExtension();
        try {
            DB::beginTransaction();
            $resource_file = ResourceFile::create($input);
            if ($request->hasFile('file')) {
                $response = Http::withHeaders(
                    ['Authorization' => config('app.api_key_satudata')])
                    ->attach('upload', fopen($request->file('file'), 'r'),
                        $input['title'] . '.' . $request->file('file')
                            ->getClientOriginalExtension())
                    ->post('https://data.kutaitimurkab.go.id/api/3/action/resource_create', $input);
                $file = $request->file('file');
                $filenames = str_replace(" ", "-", $file->getClientOriginalName());
                $path = $request->file->storeAs('public/resource_file', $filenames, 'local');
                $resource_file->file = 'storage' . substr($path, strpos($path, '/'));
                $resource_file->resource_id = $response['result']['id'];
                $resource_file->save();
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollback();
            return response('error:' . $e, 500);
        }
        return response()->json([
            'messagge' => 'Data berhasil ditambahkan',
            'status' => 'sukses',
            'data' => $resource_file], 201);
    }
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $resource_file = ResourceFile::find($id);
        if (empty($resource_file)) {
            return response()->json([
                'message' => 'Data tidak ditemukan',
                'status' => 'gagal'], 404);
        }
        $resource_file->update($input);
        return response()->json([
            'message' => 'Data berhasil di update',
            'status' => 'sukses',
            'data' => $resource_file
        ], 200);
    }

    public function destroy($id)
    {
        $resource_file = ResourceFile::find($id);
        if (empty($resource_file)) {
            return response()->json([
                'message' => 'Data tidak ditemukan',
                'status' => 'gagal'], 404);
        }
        $resource_file->delete();
        $input['id'] = $resource_file->id;
        $response = Http::withHeaders([
            'Authorization' => config('app.api_key_satudata'),
        ])->post('https://data.kutaitimurkab.go.id/api/3/action/resource_delete', $input);
        return response()->json([
            'message' => 'Data berhasil di hapus',
            'status' => 'sukses'], 200);
    }
}

