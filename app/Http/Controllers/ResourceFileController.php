<?php

namespace App\Http\Controllers;

use App\Models\Dataset;
use App\Models\ResourceFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;
use Mockery\Exception;

class ResourceFileController extends Controller
{
    public function index()
    {
        $resource_files = ResourceFile::all();
        $columns = Schema::getColumnListing('resource_file');
        $field = [];
        foreach ($columns as $column){
            array_push($field,['id'=>$column]);
        }
        return view('resource_file.index',compact('resource_files'));
    }

    public function create()
    {
        $resourceFile = new ResourceFile();
        $dataset = Dataset::all();
        return view('resource_file.create',compact('resourceFile','dataset'));
    }

    public function store(Request $request)
    {
        $input = $request->except('file');
        $dataset = Dataset::find($input['dataset_id']);
        $input['package_id'] = $dataset->package_id;
        $input['language'] = $dataset->language;
        $input['metadata_language'] = $dataset->language;
        $input['name'] = str_replace(' ', '-', strtolower($input['title']));
        $input['format'] = $request->file('file')->getClientOriginalExtension();
        try{
            DB::beginTransaction();
            $resource_file = ResourceFile::create($input);

            if( $request->hasFile('file')) {
                $response = Http::withHeaders(['Authorization' => config('app.api_key_satudata')])
                    ->attach('upload',fopen($request->file('file'),'r'),$input['title'].'.'.$request->file('file')->getClientOriginalExtension())
                    ->post('https://data.kutaitimurkab.go.id/api/3/action/resource_create', $input);
                $file = $request->file('file');
                $filenames = str_replace(" ", "_", $file->getClientOriginalName());
                $path=$request->file->storeAs('public/resource_file', $filenames,'local');
                $resource_file->file='storage'.substr($path,strpos($path,'/'));
                $resource_file->resource_id = $response['result']['id'];
                $resource_file->save();
                DB::commit();
            }
        }catch (Exception $e){
            DB::rollback();
        }
        return redirect(route('resource_files.index'));
    }

    public function edit(ResourceFile $resourceFile)
    {
        $dataset = Dataset::all();
        return view('resource_file.edit',compact('resourceFile','dataset'));
    }

    public function update(Request $request, ResourceFile $resourceFile)
    {
        $input = $request->except('file');
        try{
            DB::beginTransaction();
            $resourceFile->update($input);

            if( $request->hasFile('file')) {
                $response = Http::withHeaders(['Authorization' => config('app.api_key_satudata')])
                    ->attach('upload',fopen($request->file('file'),'r'),
                        $input['title'].'.'.$request->file('file')->getClientOriginalExtension())
                    ->post('https://data.kutaitimurkab.go.id/api/3/action/resource_update',
                        [
                            "id"=>$resourceFile->resource_id,
                            "name"=>str_replace(' ', '-', strtolower($input['title'])),
                            "format"=>$request->file('file')->getClientOriginalExtension()
                        ]);
                $file = $request->file('file');
                $filenames = str_replace(" ", "_", $file->getClientOriginalName());
                $path=$request->file->storeAs('public/resource_file', $filenames,'local');
                $resourceFile->file='storage'.substr($path,strpos($path,'/'));
                $resourceFile->save();

                DB::commit();
            }
        }catch (Exception $e){
            DB::rollback();
        }
        return redirect(route('resource_files.index'));
    }

    public function destroy(ResourceFile $resourceFile)
    {
        $resourceFile->delete();
        $input['id'] = $resourceFile->resource_id;
        $response = Http::withHeaders([
            'Authorization' => config('app.api_key_satudata'),
        ])->post('https://data.kutaitimurkab.go.id/api/3/action/resource_delete', $input);

        return redirect(route('resource_files.index'));
    }
}
