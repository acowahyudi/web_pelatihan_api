<?php

namespace App\Http\Controllers;

use App\Models\Dataset;
use App\Models\LuasPerkebunan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\VarDumper\Cloner\Data;

class LuasPerkebunanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataset = Dataset::all();
        $luas_perkebunan = LuasPerkebunan::all();
        return view('luas_perkebunan.index',compact('luas_perkebunan','dataset'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $luasPerkebunan = new LuasPerkebunan();
        return view('luas_perkebunan.create',compact('luasPerkebunan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        LuasPerkebunan::create($input);
        return redirect(route('luas_perkebunan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LuasPerkebunan  $luasPerkebunan
     * @return \Illuminate\Http\Response
     */
    public function show(LuasPerkebunan $luasPerkebunan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LuasPerkebunan  $luasPerkebunan
     * @return \Illuminate\Http\Response
     */
    public function edit(LuasPerkebunan $luasPerkebunan)
    {
        return view('luas_perkebunan.edit',compact('luasPerkebunan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LuasPerkebunan  $luasPerkebunan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LuasPerkebunan $luasPerkebunan)
    {
        $input = $request->all();
        $luas_perkebunan = LuasPerkebunan::find($luasPerkebunan->id);
        $luas_perkebunan->update($input);
        return redirect(route('luas_perkebunan.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LuasPerkebunan  $luasPerkebunan
     * @return \Illuminate\Http\Response
     */
    public function destroy(LuasPerkebunan $luasPerkebunan)
    {
        $luasPerkebunan->delete();

        return redirect(route('luas_perkebunan.index'));
    }

    public function sendToSatuData(Request $request){
        try{
            $package = Dataset::find($request->dataset_id);

            $resource['package_id'] = $package->package_id;
            $resource['language'] = $package->language;
            $resource['metadata_language'] = $package->language;
            $resource['name'] = 'Luas Perkebunan';
            $resource['title'] = 'Luas Perkebunan';
            $resource['description'] = 'Luas Perkebunan Kutim';
            $dataPerkebunan = LuasPerkebunan::all();
            foreach ($dataPerkebunan as $item){
                $item->luas_lahan = $item->luas_lahan.' Ha';
            }
            foreach ($dataPerkebunan as $item){
                unset($item['id']);
                unset($item['deleted_at']);
            }

            $columns = Schema::getColumnListing('luas_perkebunan');
            $field = [];
            foreach ($columns as $column){
                array_push($field,['id'=>$column]);
            }

            return $dataPerkebunan;

            $response = Http::withHeaders(['Authorization' => config('app.api_key_satudata')])
                ->post('https://data.kutaitimurkab.go.id/api/3/action/datastore_create',[
                    'resource' => $resource,
                    'fields' => $field,
                    'records' => $dataPerkebunan
                ]);
            return redirect(route('luas_perkebunan.index'))->with(['success'=>'Data berhasil di kirim']);
        }catch (\Exception $exception){
            return redirect(route('luas_perkebunan.index'),500)->with(['error'=>'Data gagal di kirim']);
        }
    }
}
