<?php

namespace App\Http\Controllers;

use App\Models\Dataset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class KoperasiController extends Controller
{
    public function index()
    {
        $dataset = Dataset::all();
        $koperasi = Http::get('https://koperasi.nauraweb.com/api/stat_all')->json();

        return view('koperasi.index',compact('koperasi','dataset'));
    }

    public function sendToSatuData(Request $request)
    {
        try{
            $package = Dataset::find($request->dataset_id);

            $resource['package_id'] = $package->package_id;
            $resource['language'] = $package->language;
            $resource['metadata_language'] = $package->language;
            $resource['name'] = 'Data Koperasi';
            $resource['title'] = 'Data Koperasi';
            $resource['description'] = 'Data Koperasi';
            $resource['mimetype'] = 'application/json';
            $resource['format'] = 'json';
            $dataKoperasi = Http::get('https://koperasi.nauraweb.com/api/stat_all')->json();

            $response = Http::withHeaders(['Authorization' => config('app.api_key_satudata')])
                ->post('https://data.kutaitimurkab.go.id/api/3/action/datastore_create',[
                    'resource' => $resource,
                    'fields' => $dataKoperasi['fields'],
                    'records' => $dataKoperasi['records']
                ]);
            return redirect(route('koperasi.index'))->with(['success'=>'Data berhasil di kirim']);
        }catch (\Exception $exception){
            return redirect(route('koperasi.index'),500)->with(['error'=>'Data gagal di kirim']);
        }
    }
}
